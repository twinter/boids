"""
some parts from https://github.com/fogleman/Minecraft/blob/master/main.py
"""
import main
import math
import numpy
from typing import TypeVar
import pyglet
from pyglet.gl import *
from pyglet.window import *


UPDATES_PER_SECOND = 60

TERMINAL_VELOCITY = 2.5

ACCELERATION = 10

DECELERATION = 10


class Window(pyglet.window.Window):
	def __init__(self, environment: TypeVar('Environment'), width=1024, height=768, *args, **kwargs):
		super(Window, self).__init__(resizable=True, caption='Boids', *args, **kwargs)
		self.env = environment
		
		# position (x, y, z) relative to the world
		# x: to the right
		# y: upward
		# z: forward
		self.position = (0.0, 2.0, 0.0)
		
		# the speed (x, y, z) in direction of the axes
		self.speed = (0.0, 0.0, 0.0)
		
		self.acceleration = (0.0, 0.0, 0.0)
		
		# rotation around each axis (x, y, z) in the direction used in glRotatef()
		self.rotation = (0.0, 0.0, 0.0)
		
		# movement orders relative to the camera
		# first element: 1 forward, -1 backward
		# second element: 1 left, -1 right
		self.movement = [0, 0]
		
		# if True: reports mouse position and locks mouse to this window
		# no such property for fullscreen as it exists in pyglet.window.Window
		self._exclusive = False
		self.exclusive = property(lambda self: self._exclusive, self.set_exclusive_mouse)
		
		self.windowed = property(lambda self: not self.fullscreen, self.set_windowed)
		
		self.setup_opengl()
		self.setup_fog()
		
		# The label that is displayed in the top left of the canvas.
		self.label = pyglet.text.Label('', font_name='Arial', font_size=12, x=10, y=self.height - 10, anchor_x='left',
		                               anchor_y='top', color=(0, 0, 0, 255))
		
		pyglet.clock.schedule_interval(self.update, 1.0 / UPDATES_PER_SECOND)
		
		pyglet.app.run()
	
	def setup_opengl(self):
		self.set_exclusive_mouse(self._exclusive)
		self.set_fullscreen(self._fullscreen)
		
		# Set the color of "clear", i.e. the sky, in rgba.
		glClearColor(0.9, 0.9, 0.9, 1)
		
		# Enable culling (not rendering) of back-facing facets -- facets that aren't
		# visible to you.
		glEnable(GL_CULL_FACE)
		
		# Set the texture minification/magnification function to GL_NEAREST (nearest
		# in Manhattan distance) to the specified texture coordinates. GL_NEAREST
		# "is generally faster than GL_LINEAR, but it can produce textured images
		# with sharper edges because the transition between texture elements is not
		# as smooth."
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST)
	
	def setup_fog(self):
		"""
			copied entirely from that Minecraft clone for now
		"""
		
		# Enable fog. Fog "blends a fog color with each rasterized pixel fragment's
		# post-texturing color."
		glEnable(GL_FOG)
		# Set the fog color.
		glFogfv(GL_FOG_COLOR, (GLfloat * 4)(0.5, 0.69, 1.0, 0.8))
		# Say we have no preference between rendering speed and quality.
		glHint(GL_FOG_HINT, GL_DONT_CARE)
		# Specify the equation used to compute the blending factor.
		glFogi(GL_FOG_MODE, GL_LINEAR)
		# How close and far away fog starts and ends. The closer the start and end,
		# the denser the fog in the fog range.
		glFogf(GL_FOG_START, 20.0)
		glFogf(GL_FOG_END, 60.0)
	
	
	# --- EVENTS ---
	def on_draw(self):
		pyglet.clock.tick()
		
		# reset
		self.clear()
		glColor3d(1, 1, 1)
		
		# draw 3D stuff (world)
		self.set3d()
		self.env.draw()
		
		# draw 2D stuff (gui)
		self.set2d()
		px, py, pz = self.position
		vx, vy, vz = self.speed
		ax, ay, az = self.acceleration
		self.label.text = '%02d (p: %.2f, %.2f, %.2f; v: %.2f, %.2f, %.2f; a: %.2f, %.2f, %.2f)' % \
		                  (pyglet.clock.get_fps(), px, py, pz, vx, vy, vz, ax, ay, az)
		self.label.draw()
	
	def on_key_press(self, symbol, modifier):
		if symbol == key.F:
			print('key: F')
			self.windowed = not self.windowed
			print('windowed:', self.windowed)
		elif symbol == key.W:
			self.movement[0] += 1
			self.update_acceleration()
		elif symbol == key.S:
			self.movement[0] -= 1
			self.update_acceleration()
		elif symbol == key.A:
			self.movement[1] += 1
			self.update_acceleration()
		elif symbol == key.D:
			self.movement[1] -= 1
			self.update_acceleration()
		elif symbol == key.ESCAPE:
			quit()
	
	def on_key_release(self, symbol, modifier):
		if symbol == key.W:
			self.movement[0] += -1
			self.update_acceleration()
		elif symbol == key.S:
			self.movement[0] -= -1
			self.update_acceleration()
		elif symbol == key.A:
			self.movement[1] += -1
			self.update_acceleration()
		elif symbol == key.D:
			self.movement[1] -= -1
			self.update_acceleration()
	
	
	# --- HELPERS ---
	def update(self, dt):
		"""
			called repeatedly by pyglet.clock
			triggers updates for camera position and the environment
		"""
		
		dt = min(dt, 0.2)
		
		# update world
		self.env.update(dt)
		self.update_movement(dt)
	
	def update_movement(self, dt):
		# update position
		# p = a / 2 * dt**2 + v0 * t + p0
		p = numpy.divide(self.acceleration, 2)
		p = numpy.multiply(p, dt ** 2)
		p2 = numpy.multiply(self.speed, dt)
		p = numpy.add(p, p2)
		p = numpy.add(p, self.position)
		
		# update speed 
		# v = a * dt + v0
		v = numpy.multiply(self.acceleration, dt)
		v = numpy.add(v, self.speed)
		
		# limit length of v from -TERMINAL_VELOCITY to TERMINAL_VELOCITY
		len = len0 = numpy.linalg.norm(v)
		if len > TERMINAL_VELOCITY or len < TERMINAL_VELOCITY * -1:
			len = min(len, TERMINAL_VELOCITY)
			len = max(len, TERMINAL_VELOCITY * -1)
			v = numpy.multiply(v, len / len0  )
		
		self.position = p
		self.speed = v
		
		# TODO: implement deceleration
	
	def update_acceleration(self):
		'''
			should be triggered on anything influencing self.acceleration.
			this means changes to self.movement and to self.rotation
		'''
		
		x, y, z = self.acceleration
		x = self.movement[0] * ACCELERATION
		z = self.movement[1] * ACCELERATION
		self.acceleration = (x, y, z)
		
		#TODO: do proper calculations
	
	def set_exclusive_mouse(self, exclusive: bool) -> None:
		self._exclusive = exclusive
		super().set_exclusive_mouse(exclusive)
	
	def set_windowed(self, windowed: bool) -> None:
		print('debug')
		fs = not windowed
		super().set_fullscreen(fullscreen=fs)
	
	def set2d(self):
		width, height = self.get_size()
		glDisable(GL_DEPTH_TEST)
		glViewport(0, 0, width, height)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		glOrtho(0, width, 0, height, -1, 1)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
	
	def set3d(self):
		width, height = self.get_size()
		glEnable(GL_DEPTH_TEST)
		glViewport(0, 0, width, height)
		glMatrixMode(GL_PROJECTION)
		glLoadIdentity()
		gluPerspective(90.0, width / float(height), 0.1, 100.0)
		glMatrixMode(GL_MODELVIEW)
		glLoadIdentity()
		
		# rotation w/ glRotatef(angle, x, y, z) (rotate a certain angle around the given vector)
		x, y, z = self.rotation
		glRotatef(x, 1, 0, 0)
		glRotatef(y, 0, 1, 0)
		glRotatef(z, 0, 0, 1)
		
		# translate the camera to it's position
		# TODO: does this make sense?
		x, y, z = self.position
		glTranslatef(-x, -y, -z)
