import environment
import window

def main():
	env = environment.Environment()
	w = window.Window(env)
	w.exclusive = True

if __name__ == '__main__':
	main()

